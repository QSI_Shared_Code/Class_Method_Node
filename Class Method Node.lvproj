﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="20008000">
	<Item Name="My Computer" Type="My Computer">
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="Editor" Type="Folder">
			<Item Name="Main XNode Editor.vi" Type="VI" URL="../../XNode Editor/Main XNode Editor.vi"/>
		</Item>
		<Item Name="Support VIs" Type="Folder">
			<Item Name="Test Classes" Type="Folder">
				<Item Name="Test Child 1.lvclass" Type="LVClass" URL="../Support/Test Child 1/Test Child 1.lvclass"/>
				<Item Name="Test Class.lvclass" Type="LVClass" URL="../Test Class/Test Class.lvclass"/>
			</Item>
			<Item Name="DVR Test.vi" Type="VI" URL="../Support/DVR Test.vi"/>
			<Item Name="Get Method Description.vi" Type="VI" URL="../Support/Get Method Description.vi"/>
			<Item Name="Get Property Description.vi" Type="VI" URL="../Support/Get Property Description.vi"/>
			<Item Name="Script DVR.vi" Type="VI" URL="../Support/Script DVR.vi"/>
			<Item Name="Script Set Message.vi" Type="VI" URL="../Support/Script Set Message.vi"/>
			<Item Name="Support Add Property.vi" Type="VI" URL="../Support/Support Add Property.vi"/>
			<Item Name="Support Empty Class.vi" Type="VI" URL="../Support Empty Class.vi"/>
			<Item Name="Support Terminal Bounds.vi" Type="VI" URL="../Support Terminal Bounds.vi"/>
			<Item Name="Support Test Get Methods.vi" Type="VI" URL="../Support/Support Test Get Methods.vi"/>
			<Item Name="Support Test Message.vi" Type="VI" URL="../Support Test Message.vi"/>
			<Item Name="Test VI.vi" Type="VI" URL="../Test VI.vi"/>
		</Item>
		<Item Name="TypeDefs" Type="Folder">
			<Item Name="Direction Enum.ctl" Type="VI" URL="../SubVIs/Direction Enum.ctl"/>
			<Item Name="Message Type Enum.ctl" Type="VI" URL="../Message Type Enum.ctl"/>
			<Item Name="Method Type Enum.ctl" Type="VI" URL="../Method Type Enum.ctl"/>
			<Item Name="Parameters Cluster.ctl" Type="VI" URL="../SubVIs/Parameters Cluster.ctl"/>
			<Item Name="Terminal Additional Information.ctl" Type="VI" URL="../Terminal Additional Information.ctl"/>
			<Item Name="Terminal Type.ctl" Type="VI" URL="../Terminal Type.ctl"/>
		</Item>
		<Item Name="Assign Terminal Bounds.vi" Type="VI" URL="../Assign Terminal Bounds.vi"/>
		<Item Name="Calculate Class and Error Terminal Bounds.vi" Type="VI" URL="../Calculate Class and Error Terminal Bounds.vi"/>
		<Item Name="Center Image.vi" Type="VI" URL="../Center Image.vi"/>
		<Item Name="Class Method Node Scripting API.lvlib" Type="Library" URL="../Scripting API/Class Method Node Scripting API.lvlib"/>
		<Item Name="Determine Node Width.vi" Type="VI" URL="../Determine Node Width.vi"/>
		<Item Name="Determine Required Level.vi" Type="VI" URL="../Determine Required Level.vi"/>
		<Item Name="Draw Class Banner.vi" Type="VI" URL="../Draw Class Banner.vi"/>
		<Item Name="Draw Class Method Node.vi" Type="VI" URL="../Draw Class Method Node.vi"/>
		<Item Name="Draw Constructor Glyph.vi" Type="VI" URL="../Draw Constructor Glyph.vi"/>
		<Item Name="Draw Cube Glyph.vi" Type="VI" URL="../Draw Cube Glyph.vi"/>
		<Item Name="Draw Destructor Glyph.vi" Type="VI" URL="../Draw Destructor Glyph.vi"/>
		<Item Name="Draw Direction Arrow Glyph.vi" Type="VI" URL="../Draw Direction Arrow Glyph.vi"/>
		<Item Name="Draw Error Glyph.vi" Type="VI" URL="../Draw Error Glyph.vi"/>
		<Item Name="Draw Invoke Arrow Glyph.vi" Type="VI" URL="../Draw Invoke Arrow Glyph.vi"/>
		<Item Name="Draw Method Banner.vi" Type="VI" URL="../Draw Method Banner.vi"/>
		<Item Name="Draw Parameter.vi" Type="VI" URL="../Draw Parameter.vi"/>
		<Item Name="Expected Terminals.vi" Type="VI" URL="../Expected Terminals.vi"/>
		<Item Name="Find Class CORE.vi" Type="VI" URL="../Find Class CORE.vi"/>
		<Item Name="Find Class.vi" Type="VI" URL="../Find Class.vi"/>
		<Item Name="Generate DVR Method Wrapper.vi" Type="VI" URL="../Generate DVR Method Wrapper.vi"/>
		<Item Name="Generate In Place Element Structure.vi" Type="VI" URL="../Generate In Place Element Structure.vi"/>
		<Item Name="Generate Merge Errors Node.vi" Type="VI" URL="../Generate Merge Errors Node.vi"/>
		<Item Name="Generate Method SubVI.vi" Type="VI" URL="../Generate Method SubVI.vi"/>
		<Item Name="Generate Method Wrapper.vi" Type="VI" URL="../Generate Method Wrapper.vi"/>
		<Item Name="Get Methods.vi" Type="VI" URL="../Get Methods.vi"/>
		<Item Name="Get Terminal Information (Class).vi" Type="VI" URL="../Get Terminal Information (Class).vi"/>
		<Item Name="Get Terminal Information (Errors).vi" Type="VI" URL="../Get Terminal Information (Errors).vi"/>
		<Item Name="Get Terminal Information (Parameters).vi" Type="VI" URL="../Get Terminal Information (Parameters).vi"/>
		<Item Name="Get Terminals.vi" Type="VI" URL="../Get Terminals.vi"/>
		<Item Name="Guess Method Type.vi" Type="VI" URL="../Guess Method Type.vi"/>
		<Item Name="Guess Terminal Type.vi" Type="VI" URL="../Guess Terminal Type.vi"/>
		<Item Name="Initialize Terminals.vi" Type="VI" URL="../Initialize Terminals.vi"/>
		<Item Name="Manual Terminal Definition Dialog.vi" Type="VI" URL="../Manual Terminal Definition Dialog.vi"/>
		<Item Name="Reset State.vi" Type="VI" URL="../Reset State.vi"/>
		<Item Name="Script Data Type.vi" Type="VI" URL="../Script Data Type.vi"/>
		<Item Name="Set Class.vi" Type="VI" URL="../Set Class.vi"/>
		<Item Name="Set Method.vi" Type="VI" URL="../Set Method.vi"/>
		<Item Name="Split Terminal Array.vi" Type="VI" URL="../Split Terminal Array.vi"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="user.lib" Type="Folder">
				<Item Name="Append Path to Root if Relative - Absolute or Relative Path Array__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/file/file.llb/Append Path to Root if Relative - Absolute or Relative Path Array__ogtk.vi"/>
				<Item Name="Append Path to Root if Relative - Array__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/file/file.llb/Append Path to Root if Relative - Array__ogtk.vi"/>
				<Item Name="Append Path to Root if Relative - Root Path Array__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/file/file.llb/Append Path to Root if Relative - Root Path Array__ogtk.vi"/>
				<Item Name="Append Path to Root if Relative - Scalar__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/file/file.llb/Append Path to Root if Relative - Scalar__ogtk.vi"/>
				<Item Name="Append Path to Root if Relative__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/file/file.llb/Append Path to Root if Relative__ogtk.vi"/>
				<Item Name="Strip Path - Arrays__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/file/file.llb/Strip Path - Arrays__ogtk.vi"/>
				<Item Name="Strip Path - Traditional__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/file/file.llb/Strip Path - Traditional__ogtk.vi"/>
				<Item Name="Strip Path__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/file/file.llb/Strip Path__ogtk.vi"/>
			</Item>
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Add State(s) to Queue__jki_lib_state_machine.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/State Machine/_JKI_lib_State_Machine.llb/Add State(s) to Queue__jki_lib_state_machine.vi"/>
				<Item Name="Bit-array To Byte-array.vi" Type="VI" URL="/&lt;vilib&gt;/picture/pictutil.llb/Bit-array To Byte-array.vi"/>
				<Item Name="BuildHelpPath.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="Calc Long Word Padded Width.vi" Type="VI" URL="/&lt;vilib&gt;/picture/bmp.llb/Calc Long Word Padded Width.vi"/>
				<Item Name="Check Path.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Check Path.vi"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Color to RGB.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/colorconv.llb/Color to RGB.vi"/>
				<Item Name="Control.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/QSI/QControls/QControl Classes/Control/Control.lvclass"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="Create Mask By Alpha.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Create Mask By Alpha.vi"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogType.ctl"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="Directory of Top Level VI.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Directory of Top Level VI.vi"/>
				<Item Name="Draw Flattened Pixmap.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Draw Flattened Pixmap.vi"/>
				<Item Name="Draw Line.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Draw Line.vi"/>
				<Item Name="Draw Multiple Lines.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Draw Multiple Lines.vi"/>
				<Item Name="Draw Point.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Draw Point.vi"/>
				<Item Name="Draw Rectangle.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Draw Rectangle.vi"/>
				<Item Name="Draw Text at Point.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Draw Text at Point.vi"/>
				<Item Name="Draw Text in Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Draw Text in Rect.vi"/>
				<Item Name="Empty Picture" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Empty Picture"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="ex_CorrectErrorChain.vi" Type="VI" URL="/&lt;vilib&gt;/express/express shared/ex_CorrectErrorChain.vi"/>
				<Item Name="Find Tag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find Tag.vi"/>
				<Item Name="FixBadRect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/pictutil.llb/FixBadRect.vi"/>
				<Item Name="Flip and Pad for Picture Control.vi" Type="VI" URL="/&lt;vilib&gt;/picture/bmp.llb/Flip and Pad for Picture Control.vi"/>
				<Item Name="Format Message String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Format Message String.vi"/>
				<Item Name="General Error Handler Core CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler Core CORE.vi"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="Generic.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/QSI/QControls/QControl Classes/Generic/Generic.lvclass"/>
				<Item Name="Get File Extension.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Get File Extension.vi"/>
				<Item Name="Get File System Separator.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/sysinfo.llb/Get File System Separator.vi"/>
				<Item Name="Get LV Class Path.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Path.vi"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetHelpDir.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="GObject.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/QSI/QControls/QControl Classes/GObject/GObject.lvclass"/>
				<Item Name="High Resolution Relative Seconds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/High Resolution Relative Seconds.vi"/>
				<Item Name="imagedata.ctl" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/imagedata.ctl"/>
				<Item Name="Is Path and Not Empty.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Is Path and Not Empty.vi"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="LVDataSocketStatusTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVDataSocketStatusTypeDef.ctl"/>
				<Item Name="LVKeyNavTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVKeyNavTypeDef.ctl"/>
				<Item Name="LVMouseTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVMouseTypeDef.ctl"/>
				<Item Name="LVPointTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVPointTypeDef.ctl"/>
				<Item Name="LVPositionTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVPositionTypeDef.ctl"/>
				<Item Name="LVRectTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
				<Item Name="LVRowAndColumnTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRowAndColumnTypeDef.ctl"/>
				<Item Name="Move Pen.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Move Pen.vi"/>
				<Item Name="NI_Data Type.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/Data Type/NI_Data Type.lvlib"/>
				<Item Name="NI_XML.lvlib" Type="Library" URL="/&lt;vilib&gt;/xml/NI_XML.lvlib"/>
				<Item Name="NI_XNodeSupport.lvlib" Type="Library" URL="/&lt;vilib&gt;/XNodeSupport/NI_XNodeSupport.lvlib"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="Parse State Queue__jki_lib_state_machine.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/State Machine/_JKI_lib_State_Machine.llb/Parse State Queue__jki_lib_state_machine.vi"/>
				<Item Name="PCT Pad String.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/PCT Pad String.vi"/>
				<Item Name="Picture to Pixmap.vi" Type="VI" URL="/&lt;vilib&gt;/picture/pictutil.llb/Picture to Pixmap.vi"/>
				<Item Name="Picture.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/QSI/QControls/QControl Classes/Picture/Picture.lvclass"/>
				<Item Name="Qualified Name Array To Single String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Qualified Name Array To Single String.vi"/>
				<Item Name="Random Number (Range) DBL.vi" Type="VI" URL="/&lt;vilib&gt;/numeric/Random Number (Range) DBL.vi"/>
				<Item Name="Random Number (Range) I64.vi" Type="VI" URL="/&lt;vilib&gt;/numeric/Random Number (Range) I64.vi"/>
				<Item Name="Random Number (Range) U64.vi" Type="VI" URL="/&lt;vilib&gt;/numeric/Random Number (Range) U64.vi"/>
				<Item Name="Random Number (Range).vi" Type="VI" URL="/&lt;vilib&gt;/numeric/Random Number (Range).vi"/>
				<Item Name="Read BMP File Data.vi" Type="VI" URL="/&lt;vilib&gt;/picture/bmp.llb/Read BMP File Data.vi"/>
				<Item Name="Read BMP File.vi" Type="VI" URL="/&lt;vilib&gt;/picture/bmp.llb/Read BMP File.vi"/>
				<Item Name="Read BMP Header Info.vi" Type="VI" URL="/&lt;vilib&gt;/picture/bmp.llb/Read BMP Header Info.vi"/>
				<Item Name="Read JPEG File.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Read JPEG File.vi"/>
				<Item Name="Read PNG File.vi" Type="VI" URL="/&lt;vilib&gt;/picture/png.llb/Read PNG File.vi"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="Set Busy.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Busy.vi"/>
				<Item Name="Set Cursor (Cursor ID).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor (Cursor ID).vi"/>
				<Item Name="Set Cursor (Icon Pict).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor (Icon Pict).vi"/>
				<Item Name="Set Cursor.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor.vi"/>
				<Item Name="Set Pen State.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Set Pen State.vi"/>
				<Item Name="Set String Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set String Value.vi"/>
				<Item Name="Simple Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Simple Error Handler.vi"/>
				<Item Name="sub_Random U32.vi" Type="VI" URL="/&lt;vilib&gt;/numeric/sub_Random U32.vi"/>
				<Item Name="subFile Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/express/express input/FileDialogBlock.llb/subFile Dialog.vi"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="TRef Traverse.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/traverseref.llb/TRef Traverse.vi"/>
				<Item Name="TRef TravTarget.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/traverseref.llb/TRef TravTarget.ctl"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="Unset Busy.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Unset Busy.vi"/>
				<Item Name="VI Scripting - Traverse.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/traverseref.llb/VI Scripting - Traverse.lvlib"/>
				<Item Name="VIAnUtil Check Type If ErrClust.vi" Type="VI" URL="/&lt;vilib&gt;/addons/analyzer/_analyzerutils.llb/VIAnUtil Check Type If ErrClust.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
			</Item>
			<Item Name="Add Ability.vi" Type="VI" URL="../../XNode Editor/Utilities/XNode Functions/Add Ability.vi"/>
			<Item Name="Array Selection.vim" Type="VI" URL="../Array Selection.vim"/>
			<Item Name="Class Method Node.AutotoolRegions.vi" Type="VI" URL="../Class Method Node.AutotoolRegions.vi"/>
			<Item Name="Class Method Node.BuildMenu5.vi" Type="VI" URL="../Class Method Node.BuildMenu5.vi"/>
			<Item Name="Class Method Node.Find Terminal by ID.vi" Type="VI" URL="../Class Method Node.Find Terminal by ID.vi"/>
			<Item Name="Class Method Node.GetErrors3.vi" Type="VI" URL="../Class Method Node.GetErrors3.vi"/>
			<Item Name="Class Method Node.Help.vi" Type="VI" URL="../Class Method Node.Help.vi"/>
			<Item Name="Class Method Node.ReplaceSelf.vi" Type="VI" URL="../Class Method Node.ReplaceSelf.vi"/>
			<Item Name="Class Method Node.SelectMenu5.vi" Type="VI" URL="../Class Method Node.SelectMenu5.vi"/>
			<Item Name="Class Method Node.xnode" Type="XNode" URL="../Class Method Node.xnode"/>
			<Item Name="Convert File Extension (Path)__ogtk.vi" Type="VI" URL="/../../../Program Files (x86)/National Instruments/LabVIEW 2015/user.lib/_OpenG.lib/file/file.llb/Convert File Extension (Path)__ogtk.vi"/>
			<Item Name="Convert File Extension__ogtk.vi" Type="VI" URL="/../../../Program Files (x86)/National Instruments/LabVIEW 2015/user.lib/_OpenG.lib/file/file.llb/Convert File Extension__ogtk.vi"/>
			<Item Name="Create XNode.vi" Type="VI" URL="../../XNode Editor/Utilities/XNode Functions/Create XNode.vi"/>
			<Item Name="DOMUserDefRef.dll" Type="Document" URL="DOMUserDefRef.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="Get Ability Template Name.vi" Type="VI" URL="../../XNode Editor/Utilities/Get Ability Template Name.vi"/>
			<Item Name="Get Possible Abilities.vi" Type="VI" URL="../../XNode Editor/Utilities/XNode Functions/Get Possible Abilities.vi"/>
			<Item Name="Get XNode Properties.vi" Type="VI" URL="../../XNode Editor/Utilities/Get XNode Properties.vi"/>
			<Item Name="Read XNode Members.vi" Type="VI" URL="../../XNode Editor/Utilities/XNode Functions/Read XNode Members.vi"/>
			<Item Name="Read XNode SubVIs.vi" Type="VI" URL="../../XNode Editor/Utilities/XNode Functions/Read XNode SubVIs.vi"/>
			<Item Name="Relationship Enum.ctl" Type="VI" URL="../Relationship Enum.ctl"/>
			<Item Name="Reorder 1D Array2 (Path)__ogtk.vi" Type="VI" URL="/../../../Program Files (x86)/National Instruments/LabVIEW 2015/user.lib/_OpenG.lib/array/array.llb/Reorder 1D Array2 (Path)__ogtk.vi"/>
			<Item Name="Reorder 1D Array2 (String)__ogtk.vi" Type="VI" URL="/../../../Program Files (x86)/National Instruments/LabVIEW 2015/user.lib/_OpenG.lib/array/array.llb/Reorder 1D Array2 (String)__ogtk.vi"/>
			<Item Name="Reorder Array2__ogtk.vi" Type="VI" URL="/../../../Program Files (x86)/National Instruments/LabVIEW 2015/user.lib/_OpenG.lib/array/array.llb/Reorder Array2__ogtk.vi"/>
			<Item Name="Set XNode Properties.vi" Type="VI" URL="../../XNode Editor/Utilities/Set XNode Properties.vi"/>
			<Item Name="Slideshow.lvclass" Type="LVClass" URL="../../QControls Extended/Slideshow/Slideshow.lvclass"/>
			<Item Name="Sort 1D Array (String)__ogtk.vi" Type="VI" URL="/../../../Program Files (x86)/National Instruments/LabVIEW 2015/user.lib/_OpenG.lib/array/array.llb/Sort 1D Array (String)__ogtk.vi"/>
			<Item Name="Sort Array__ogtk.vi" Type="VI" URL="/../../../Program Files (x86)/National Instruments/LabVIEW 2015/user.lib/_OpenG.lib/array/array.llb/Sort Array__ogtk.vi"/>
			<Item Name="Split String After First Letter.vi" Type="VI" URL="../../XNode Editor/Utilities/Split String After First Letter.vi"/>
			<Item Name="Split String Letter Number.vi" Type="VI" URL="../../XNode Editor/Utilities/Split String Letter Number.vi"/>
			<Item Name="Transition.lvclass" Type="LVClass" URL="../../QControls Extended/Slideshow/Transition/Transition.lvclass"/>
			<Item Name="Update State Control Types.vi" Type="VI" URL="../../XNode Editor/Utilities/XNode Functions/Update State Control Types.vi"/>
		</Item>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
</Project>
